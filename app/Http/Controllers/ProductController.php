<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = new Product;
        return response()->json(array('error'=>'', 'message'=>$product::all()), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name' => 'Invalid product name',
            'price' => 'Invalid product price',
            'pict.*' => 'Invalid product pict',
        ];
        $validator = Validator::make($request->all(), [
                'pict' => 'required|image',
                'name' => 'required',
                'price' => 'numeric'
            ], $messages);

        if ($validator->fails()) {
            return response()->json(array('error'=>'validation', 'message'=>$validator->errors()), 400);
        }

        DB::beginTransaction();
        try {
            $product = new Product;
            $product->name = trim($request->name);
            $product->price = $request->price;
            $product->pict = Storage::disk('public')->put('images', $request->pict);
            $product->user_id = Auth::id();
            $product->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(array('error'=>'database', 'message'=>$e->getMessage()), 400);
        }
        DB::commit();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name' => 'Invalid product name',
            'price' => 'Invalid product price',
            'pict' => 'Invalid product pict',
        ];
        $validator = Validator::make($request->all(), [
                'pict' => 'required|image',
                'name' => 'required',
                'price' => 'numeric'
            ], $messages);

        if ($validator->fails()) {
            return response()->json(array('error'=>'validation', 'message'=>$validator->errors()), 400);
        }

        DB::beginTransaction();
        try {
            $product = Product::find($id);
            $product->name = trim($request->name);
            $product->price = $request->price;
            $product->pict = Storage::disk('public')->put('images', $request->pict);
            $product->user_id = Auth::id();
            $product->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(array('error'=>'database', 'message'=>$e->getMessage()), 400);
        }
        DB::commit();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        try {
            $product->delete();
        } catch (Exception $e) {
            return response()->json(array('error'=>'database', 'message'=>$e->getMessage()), 400);
        }
    }

    /**
     * Show main product
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('product.index');
    }

}
